#

set -e
file="$1";
fname="${file##*/}";
bname="${fname%.*}";

gvdir='/usr/bin';
$gvdir/dot -Tpng -o "img/${bname}_dot.png" $file
$gvdir/neato -Tpng -o "img/${bname}_n.png" $file
$gvdir/twopi -Tpng -o "img/${bname}_pi.png" $file
#dotfy

true;
