/* script to replace a wililink w/ a link to the page of the same name ! */

var head = document.getElementsByTagName('head')[0];
var bod = document.getElementsByTagName('body')[0];

const map = {

 "https://gitmind.com":"https://app.gitmind.com/my",
 "https://github.com/holoKin/":"https://gitlab.com/kin4/",
 "https://www.ipfs.com/":"http://yoogle.com:5001/ipns/webui.ipfs.io/#/files",
 "https://telegram.org/":"http://web.telegram.org/",
 "https://conceptboard.com/":"https://app.conceptboard.com/boards#",
 "https://www.bublup.com/":"https://mystuff.bublup.com/mybublup/#/mystuff/top/folder",
 "https://www.clickup.com/":"https://app.clickup.com/",
 "https://workflowy.com/":"https://workflowy.com/#/7db22a57d98c",
 "https://holoversity.gq":"https://qwant.com/?q=%26b+%2BholoVersity",
 "https://holokrysthal.gq":"https://qwant.com/?q=%26g+%2BholoVerse+Krysthal+architecture",

 "http://yoogle.com:8088/testing/calendar":"https://calendar.google.com/calendar/r",
 "holoZoom":"https://zoom.us/j/732-581-5149",
 "minichain":"https://zoom.us/j/305-649-8482",
 "timezone":"https://www.worldtimebuddy.com/"
}


var links = bod.getElementsByTagName('a');
for (let i = 0; i < links.length; i++) {
  let href=links[i].href;
  if (typeof(map[href]) != 'undefined') {
    console.log('link:'+href+' -> '+map[href]);
    links[i].href = map[href];
  }
}
