---
layout: default
---
# holoWiki

![status](https://img.shields.io/gitlab/pipeline/kin4/holowiki/master?style=flat-square)

[[holowiki]] is a very simple wiki (text based)
therefore is universal, open, interopable and future proof !

It uses smart-wiki-link to access the [holoRing](https://duckduckgo.com/?q=%22holoRing%22+blockchain) blockchain
where the data is universally stored.


## Where to find it ?

Its location is <https://kin4.gitlab.io/holowiki/> and on gitlab [pages][1]

you can browse the Wiki from its top page : [index.md](index) or directly
from [gitlab.com/kin4/holowiki.wiki.git][2]


read more: [holowiki.md](holowiki) and [gitlab wiki](https://gitlab.com/kin4/holowiki/-/wikis/home)
.

### file repository

This repository is replicated in 3 mutable locations :

1. framagit: <https://framagit.org/kin/holowiki>, where the development work is achieved
2. gitlab: <https://gitlab.com/kin4/holowiki> where the production releases resides
3. gihub: <https://github.com/holoKIN/holoWiki/> where an encrypted archive resides


### runners !

* [/jobs](https://gitlab.com/kin4/holowiki/-/jobs)
* [/pipelines](https://gitlab.com/kin4/holowiki/-/pipelines)

[1]: https://kin4.gitlab.io/holowiki/
[2]: https://gitlab.com/kin4/holowiki.wiki.git

<noscript>{this is a javascrip test !}</noscript><script> console.log('123 testing)</script>
