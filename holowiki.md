---
layout: default
---
# Traveling the Collective Knowledge ...

*HoloWikis* is opening the [[glocal]] collective knowledge to anyone
using the our [[PCE|PlanetaryOS for Conscious Evolution]]
or anyone in the [[HuRing]] network.

holowiki that the wiki to it core function and do nothing else,
however it does it locally on a [[MyC-Helium]] blockchain (v4.0).
so allow the wiki to be fractal and federated !

So what is the core function of a wiki ?

wiki == a page containing [[wikiLink]]s.

wikiLink == a [[keyword]] enclosed with "\[[" and "\]]" referencing globally resolvable resources

wikiLink are resolved locally then glocally on the [[holoRing]]
then globally using [[meta-search|meta-search engine]] and finally
using #hashtags on [[social-media]].

If the target page doesn't exist it then creates the corresponding file
into the local holowiki and offer to edit it.

You can interlink hollowikis using an URI like [[michel@holowiki:interlink]]
where "michel@holowiki" is a known identity to the [[holoSphere]].

the wikilinks are resolved with the javascript from [[@iggy]]: 
 https://cdn.jsdelivr.net/gh/iglake/js@2.0.0/dist/smart-links.js
 

```
[[keywords]]
#hashtag
@person
%context
?m=holomap+discovery+query
?q=holowiki+search+query

```

## other pages :

 * [holoMap](holoMap.html)
 * [holoNav](navigation.html)
 * [holoFlow](holoFlow.html)
 * [holodict](holodict.yml)
 * [holoWiki](holoWiki.pdf)
 * [gitlabWiki](https://gitlab.com/kin4/holowiki.wiki.git)
