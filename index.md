---
layout: default
---
# holoWiki

[holowiki][GHW] is a very simple wiki (text based)
therefore is universal, open, interopable and future proof !

The power of it lies on the fact that you can interlink wiki-pages just
as simply as setting a url classical [[weblink]] to the [[holoVerse]] ([[web4|web 4.0]]) 

* [gitlab wiki](https://gitlab.com/kin4/holowiki.wiki.git)
* [github wiki][GHW]
* [wiki/home](wiki/home.md) or [wiki/index](wiki/index.html)
* [README.md](README.html)
* [Architecture](architecture.html)
* [holowiki](holowiki.html)
* [holoMap](holoMap.html)
* [navigation](navigation.html)

more about [[@michelc]]
.

[GHW]: https://gradual-quanta.gitlab.io/qwiki/#holowiki
