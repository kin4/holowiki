---
layout: default
---
## Architecture of the holoSphere

components, applications, packages, kits, plugins, add-ons, protocols

### Dashboard components

#### User UX

 - [holoBoard]
 - holoGate ([interoperability])
 - holoCode ([Code of Conduct])
 - holoContract ([resonance contract])
 - holoGuides
 - [holoHub] (holoStore for "cards")
 - [holoConnect] (RTC comm.)
 - holoMemory (long term memory)
 - [holoMaps]
 - [holoLicense] (unlicense 4.0 / 5.0)
 - [holoSense]
 - [holoGov]
 - holoKit
 - holoHome (user profile)
 - holoStamp (time space trajectory ([spot]))
 - holoPassport (SSI, PKI, oAuth)
 - [holoWiki]
 - [holoRing]
 - [holoBots]
 - [holoBridge]
 - [holoValueSystem]
 - [holoHealth]
 - [holoTools]
 - [holoView]
 - [holoGraph]
 - [holoRoot]
 - [holoCryption]
 - [holoMonitor]
 - [holoPad]
 - [holoMedia]
 - [holoMemes]
 - [holoNotes]

#### Application Layers:

- HoloSeed (MLP)
   - Wiki - holoLink
   - Git repository - holoRepo
   - Guidelines - holoGuides
   - Creative License
- HoloHeart
- HoloMind
- HoloVerse
- HoloScape


#### Holoverse OS 

* holoHeart (core system)
* UI (Interface Plugins) 
* HoloMonitor 
* holoRing ledger
* holoBot Smart Contract
* HoloID (sovereign identity / holopassport, universal oAuth)
* holoCryption (security / encryption)
* holoKeys (PKI)
* holoValue System (token, coSense, etc. CSAP, currency generator - stabilizer)
* holoBridge (interoperability)
* holoCode (living code)
* Chii Ai Assistant 
* Collaboratory (holotoolkit to create)


### use cases

* P2P Encrypted Communications
* Semantic File Systems / Knowledge Network Repositories
* Collective Memory 
* Unified Conceptual Spaces
* Network Ecosystem Optimization 
* Collective Governance 
* Evolutionary Capacity Building Tools/ Processes 
* Negentropic / Entropic Energy Regulation Protocol Layer
* P2P Publishing / CoSensing
* Conscious Value System - Energy Exchange / Offer Networks
* P2P Mesh Network Internet Grid 
* Interoperable Systems / Applications/ Protocols 


### Distribution flow chart

* [holoPaper](https://docs.google.com/document/d/1e_feUAG1X4H9mtysAI8JRYw6dpQiSWnLSoaoPe1fyhs/view)
* [holoMap]
* [holoSphereSite](https://holoSphere.gq)
* [holoRepository](https://🌕.gitlab.com/overview)
* [holoMemery]
  - [LikeInMind]
  - [holoWiki]
* [holoBot]
  - [holoContracts]
  - [holoChat]
* [Videos / Tutorials][tutos]



